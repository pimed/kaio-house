class Planet {
    constructor() {
        this.meshes = [];
        this.createPlanet();
    }

    createPlanet() {
        // Leafs
        let geometrySphere = new THREE.SphereGeometry(25, 50, 50);
        let textureSphere = new THREE.TextureLoader().load(`../resources/grass.png` );
        let materialSphere = new THREE.MeshPhongMaterial({map: textureSphere});
        let sphere = new THREE.Mesh(geometrySphere, materialSphere);
        this.meshes.push(sphere);

        let geometryTorus = new THREE.TorusGeometry(22.4, 3, 15, 100);
        let textureTorus = new THREE.TextureLoader().load(`../resources/rocks.png` );
        let materialTorus = new THREE.MeshPhongMaterial({map: textureTorus});
        let torus = new THREE.Mesh(geometryTorus, materialTorus);
        torus.rotation.x = 0.5;
        this.meshes.push(torus);
    }

    setPosition(position) {
        this.mesh.position.set(position[0], position[1], position[2]);
    }
};