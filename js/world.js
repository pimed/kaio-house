class World {

    // World constructor
    constructor() {
        this.scene = null;
        this.camera = null;
        this.renderer = null;
        this.texture = null;
        this.controls = null;
        this.ambientLight = null;
        this.light = null;
    }

    // Initialize world parameters
    init() {

        // Creating a scene
        this.scene = new THREE.Scene();
        this.scene.backgroundColor = new THREE.Color(0xff0000);
        
        // Add a camera
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 1000);
        this.camera.position.x = -50
        this.camera.position.y = 50
        this.camera.position.z = 33.3 

        
        // Creating a renderer
        this.renderer = new THREE.WebGLRenderer({antialias: true});    
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(this.renderer.domElement);

        // Adding stars fond    
        {
            const loader = new THREE.TextureLoader();
            this.texture = loader.load(
                '../resources/morado2.png',
                () => {
                const rt = new THREE.WebGLCubeRenderTarget(this.texture.image.height);
                rt.fromEquirectangularTexture(this.renderer, this.texture);
                this.scene.background = rt.texture;
                });
        }

        // Adding controls
        this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        
        //light
        this.light = new SpotLight(0xFFFFFF, 0.75, [0, 200, 0]);
        this.light.setTargetPosition([0,0,0]);
        this.light.addToScene(this.scene);
        this.light.createHelper();
        //this.light.addHelperToScene(this.scene);
        this.light.initGUI();
        //console.log(this.light);
        this.ambientLight = new AmbientLight(0xFFFFFF, 0.25);
        this.ambientLight.addToScene(this.scene);
        this.ambientLight.initGUI();
    }

    addCompositeObjectToWorld(object) {
        object.meshes.forEach(element => {
            this.scene.add(element)
        });
    }

    addSimpleObjectToWorld(object) {
        this.scene.add(object.mesh);
    }


};