const world = new World();
const tree = new Tree();
const tree1 = new Tree();
const tree2 = new Tree();
const planet = new Planet();

// HOUSE
let geometryHouse = new THREE.SphereGeometry(5, 32, 32 );
let textureHouse = new THREE.TextureLoader().load(`../resources/block.png`);
let materialHouse = new THREE.MeshPhongMaterial({map: textureHouse});
let house = new THREE.Mesh(geometryHouse, materialHouse);
house.rotation.x = 1;

let geometryDoor = new THREE.BoxGeometry(1, 2, 2);
let materialDoor = new THREE.MeshPhongMaterial({color: 0x4D4574});
let door = new THREE.Mesh(geometryDoor, materialDoor);
door.position.set(0, 16, 19);
door.rotation.x = 1.3;

let geometryWindow1 = new THREE.TorusGeometry(2, 0.5, 15, 100);
let materialWindow1 = new THREE.MeshPhongMaterial({color: 0x4D4574});
let window1 = new THREE.Mesh(geometryWindow1, materialWindow1);
window1.position.set(1.8, 14, 22.5);
window1.rotation.x = 2.5

let geometryWindow2 = new THREE.TorusGeometry(2, 0.5, 15, 100);
let materialWindow2 = new THREE.MeshPhongMaterial({color: 0x4D4574});
let window2 = new THREE.Mesh(geometryWindow2, materialWindow2);
window2.position.set(-1.8, 14, 22.5);
window2.rotation.x = 2.5

let geometryGarage = new THREE.CylinderGeometry(3, 3, 8, 32);
let textureGarage = new THREE.TextureLoader().load(`../resources/block.png`);
let materialGarage = new THREE.MeshPhongMaterial({map:textureGarage});
let garage = new THREE.Mesh(geometryGarage, materialGarage);
garage.position.set(-12, 12, 19);
garage.rotation.x = 2.5;

let geometryWindow3 = new THREE.BoxGeometry( 2, 1, 1 );
let materialWindow3 = new THREE.MeshPhongMaterial({color:0x4D4574} );
let window3 = new THREE.Mesh(geometryWindow3, materialWindow3);
window3.position.set(-12.3, 15.8, 18);
window3.rotation.x = 2.5;
window3.rotation.y = 0.4;

let geometryTree3 = new THREE.CylinderGeometry(1 , 1, 4, 32);
let textureTree3 = new THREE.TextureLoader().load(`../resources/wood.jpg`);
let materialTree3 = new THREE.MeshPhongMaterial({map: textureTree3});
let tree3 = new THREE.Mesh(geometryTree3, materialTree3);
tree3.position.set(-23, 10, 0);
tree3.rotation.z = 1;

let geometryLeaf = new THREE.DodecahedronGeometry(4, 0);
let textureLeaf = new THREE.TextureLoader().load(`../resources/leaf.png` );
let materialLeaf = new THREE.MeshPhongMaterial({map: textureLeaf});
let leaf = new THREE.Mesh(geometryLeaf, materialLeaf);
leaf.position.set(-27, 13, 0);

// Base
let geometryBase = new THREE.CylinderGeometry(15, 15, 2, 32);
let textureBase = new THREE.TextureLoader().load(`../resources/grass1.jpg` );
let materialBase = new THREE.MeshPhongMaterial({map: textureBase});
let base = new THREE.Mesh(geometryBase, materialBase);
let positionBase = [0, 16, -60];
base.position.set(0, 16, -60);

let geometryTree4 = new THREE.CylinderGeometry(1, 1, 4, 32);
let textureTree4 = new THREE.TextureLoader().load(`../resources/wood.jpg`);
let materialTree4 = new THREE.MeshPhongMaterial({map: textureTree4});
let tree4 = new THREE.Mesh(geometryTree4, materialTree4);
tree4.position.set(-10, 17, -60);

let geometryLeaf2 = new THREE.DodecahedronGeometry(4, 0);
let textureLeaf2 = new THREE.TextureLoader().load(`../resources/leaf.png` );
let materialLeaf2 = new THREE.MeshPhongMaterial({map: textureLeaf2});
let leaf2 = new THREE.Mesh(geometryLeaf2, materialLeaf2);
leaf2.position.set(-10, 22, -60);


world.init();
tree.setPosition([0, 24.5, 0]);
tree1.setPosition([0, 0, 25]);
tree2.setPosition([0, -25, 0]);
tree1.setInclination([Math.PI / 2, 0, 0]);
tree2.setInclination([0, 0, Math.PI]);
house.position.set(0, 12, 20);

world.addCompositeObjectToWorld(tree);
world.addCompositeObjectToWorld(tree1);
world.addCompositeObjectToWorld(tree2);
world.addCompositeObjectToWorld(planet);
world.scene.add(house);
world.scene.add(door);
world.scene.add(window1);
world.scene.add(window2);
world.scene.add(garage);
world.scene.add(window3);
world.scene.add(tree3);
world.scene.add(leaf);
world.scene.add(base);
world.scene.add(tree4);
world.scene.add(leaf2);

// Game loop
function animate() {
    requestAnimationFrame(animate);

    world.renderer.render(world.scene, world.camera);
}

// Making responsive window
function onWindowResize() {
    world.camera.aspect = window.innerWidth / window.innerHeight;
    world.camera.updateProjectionMatrix();
    world.renderer.setSize(window.innerWidth, window.innerHeight);
}


function casita(position) {
    //cubo
    const cubeSize = 5;
    geometry_cube = new THREE.BoxGeometry(cubeSize, cubeSize, cubeSize);
    material_1 = new THREE.MeshPhongMaterial({ color: "rgb(252, 224, 96)" });
    cube = new THREE.Mesh(geometry_cube, material_1);
    cube.position.set(position[0], position[1] + 3, position[2]);
    world.scene.add(cube);

    //cono
    const coneSize = 7;
    geometry_cone = new THREE.ConeGeometry(coneSize / 1.4, coneSize / 2, 1000);
    material_cone = new THREE.MeshBasicMaterial({ color: "rgb(102, 84, 31)" });
    cone = new THREE.Mesh(geometry_cone, material_cone);
    cone.position.set(position[0], position[1] + 7, position[2]);
    world.scene.add(cone);

    //puerta
    geometry_door = new THREE.BoxGeometry(3, 4, 0.2);
    material_door = new THREE.MeshPhongMaterial({ color: "rgb(102, 84, 31)" });
    door = new THREE.Mesh(geometry_door, material_door);
    door.position.set(position[0], position[1] + 2, position[2] + 2.5);
    world.scene.add(door);

    //handle
    const sphereRadius = 0.3;
    geometry_sphere = new THREE.SphereGeometry(sphereRadius, 32, 32);
    material_2 = new THREE.MeshPhongMaterial({ color: "rgb(147, 230, 197)" });
    sphere = new THREE.Mesh(geometry_sphere, material_2);
    sphere.position.set(position[0] - 0.7, position[1] + 2.1, position[2] + 2.5);
    world.scene.add(sphere);

    //window1
    geometry_window_1 = new THREE.BoxGeometry(0.2, 2, 3);
    material_window_1 = new THREE.MeshPhongMaterial({ color: "rgb(147, 230, 197)" });
    window_1 = new THREE.Mesh(geometry_window_1, material_window_1);
    window_1.position.set(position[0] - 2.5, position[1] + 3, position[2]);
    world.scene.add(window_1);
}

casita(positionBase);

animate();
window.addEventListener('resize', onWindowResize, false);
