function makeXYZGUI(gui, vector3, name, onChangeFn) {
    const folder = gui.addFolder(name);
    folder.add(vector3, 'x', -200, 200).onChange(onChangeFn);
    folder.add(vector3, 'y', -200, 200).onChange(onChangeFn);
    folder.add(vector3, 'z', -200, 200).onChange(onChangeFn);
    folder.open();
}      

// utilizado por GUI para modificar los parametros de light en la scena
class ColorGUIHelper {
    constructor(object, prop) {
        this.object = object;
        this.prop = prop;
    }
    get value() {
        return `#${this.object[this.prop].getHexString()}`;
    }
    set value(hexString) {
        this.object[this.prop].set(hexString);
    }
}
// para convertir de radiaanes a grados
class DegRadHelper {
    constructor(obj, prop) {
    this.obj = obj;
    this.prop = prop;
    }
    get value() {
        return THREE.MathUtils.radToDeg(this.obj[this.prop]);
    }
    set value(v) {
        this.obj[this.prop] = THREE.MathUtils.degToRad(v);
    }
}

class Light{
    constructor(color, intensity, position){
        this.color = color;//hexadecimal
        this.intensity = intensity;
        this.light = null;
        this.position = position;
        this.targetPosition = null;
        this.gui = null;
        this.helper = null;
        this.updateLight = null;
    }
    addToScene(scene){
        scene.add(this.light);
    }    
    initGUI(){
        this.gui = new dat.GUI();
        this.gui.addColor(new ColorGUIHelper(this.light, 'color'), 'value').name('color');
        this.gui.add(this.light, 'intensity', 0, 2, 0.01);
   }
    setPosition(){
        this.light.position.set(this.position[0], this.position[1], this.position[2]);
    }
    addHelperToScene(scene){
        scene.add(this.helper);
    }
}

class LightWithTarget extends Light{
    constructor(color, intensity, position){
        super(color, intensity, position);
        this.updateLight = ()=>{
            this.light.target.updateMatrixWorld();
            this.helper.update();
        }
    }
    setTargetPosition(position){
        this.targetPosition = position;
        this.light.target.position.set(this.targetPosition[0], this.targetPosition[1], this.targetPosition[2]);
    }
    addToScene(scene){
        super.addToScene(scene);
        scene.add(this.light.target);
    }
    initGUI(){
        super.initGUI();
        makeXYZGUI(this.gui, this.light.position, 'position', this.updateLight);
        makeXYZGUI(this.gui, this.light.target.position, 'target', this.updateLight); 
    }
}

class AmbientLight extends Light{
    constructor(color, intensity){
        super(color, intensity, null);
        this.light = new THREE.AmbientLight(this.color, this.intensity);
    }
}
class HemisphereLight extends Light{
    constructor(color,colorGround, intensity){
        super(color, intensity, null);
        this.colorGround = colorGround;
        this.light = new THREE.HemisphereLight(this.color, this.colorGround, this.intensity);
    }
    initGUI(){
        super.initGUI();
        this.gui.addColor(new ColorGUIHelper(this.light, 'groundColor'), 'value').name('color');
    }
}
class DirectionalLight extends LightWithTarget{
    constructor(color, intensity, position){
        super(color, intensity, position);
        this.light = new THREE.DirectionalLight(this.color, this.intensity);
        this.setPosition();
    }
    createHelper(){
        this.helper = new THREE.DirectionalLightHelper(this.light);
    }
}
class PointLight extends Light{
    constructor(color, intensity, position){
        super(color, intensity, position);
        this.light = new THREE.PointLight(this.color, this.intensity);
        this.setPosition();
        this.updateLight = () => { this.helper.update();}
    }
    initGUI(){
        super.initGUI();
        this.gui.add(this.light, 'distance', 0, 400).onChange(this.updateLight);
        makeXYZGUI(this.gui, this.light.position, 'position', this.updateLight);
    }
    createHelper(){
        this.helper = new THREE.PointLightHelper(this.light);
    }
}
class SpotLight extends LightWithTarget{
    constructor(color, intensity, position){
       super(color, intensity, position);
       this.light = new THREE.SpotLight(this.color, this.intensity);
       this.setPosition();
    }
    initGUI(){
        super.initGUI();
        this.gui.add(this.light, 'distance', 0, 400).onChange(this.updateLight);
        this.gui.add(new DegRadHelper(this.light, 'angle'), 'value', 0, 90).name('angle').onChange(this.updateLight);
    }
    createHelper(){
        this.helper = new THREE.DirectionalLightHelper(this.light);
    }
}

//new light
//set target position si se puede
//addToscene
//createhelper si se desea
//addhelpertoscene
//initgui