class Tree {
    constructor() {
        this.meshes = [];
        this.createTree();
    }

    createTree() {
        var position = 0;
        var radius = 1.5;
        var height = 2;

        // Leafs
        for(let i = 0; i < 4; ++i) {
            let geometry = new THREE.ConeGeometry(radius, height, 10);
            let texture = new THREE.TextureLoader().load(`../resources/leaf.png`);
            let material = new THREE.MeshPhongMaterial({map: texture});
            let mesh = new THREE.Mesh(geometry, material);
            mesh.position.y = position;
            position -= 1;
            radius += 0.5;
            height += 1;
            this.meshes.unshift(mesh);
        }

        // Base
        let geometry = new THREE.CylinderGeometry(radius / 5, radius / 4, 1.5, 32);
        let texture = new THREE.TextureLoader().load(`../resources/wood.jpg`);
        let material = new THREE.MeshPhongMaterial({map: texture});
        let mesh = new THREE.Mesh(geometry, material);
        mesh.position.y = position - 2;
        this.meshes.unshift(mesh);
    }
    
    setPosition(position) {
        position[1] += 0.75;
        this.meshes[0].position.x = position[0];
        this.meshes[0].position.y = position[1];
        this.meshes[0].position.z = position[2];
        position[1] += 3;

        for(let i = 1; i < this.meshes.length; ++i) {
            this.meshes[i].position.x = position[0];
            this.meshes[i].position.y = position[1];
            this.meshes[i].position.z = position[2];
            position[1] += 1;
        }
    }

    setInclination(angle) {
        let posZ = this.meshes[0].position.z + 3;
        let posY = this.meshes[0].position.y - 4;
        this.meshes[0].rotation.x = angle[0];
        this.meshes[0].rotation.y = angle[1];
        this.meshes[0].rotation.z = angle[2];

        for(let i = 1; i < this.meshes.length; ++i) {
            this.meshes[i].rotation.x = angle[0];
            this.meshes[i].rotation.y = angle[1];
            this.meshes[i].rotation.z = angle[2];
            
            if(angle[0] != 0) {
                this.meshes[i].position.x = this.meshes[0].position.x;
                this.meshes[i].position.y = this.meshes[0].position.y;
                this.meshes[i].position.z = posZ;
                posZ += 1;
            }
            if(angle[2] != 0) {
                this.meshes[0].position.y = -25.5;
                this.meshes[i].position.x = this.meshes[0].position.x;
                this.meshes[i].position.y = posY;
                this.meshes[i].position.z = this.meshes[0].position.z;
                posY -= 1;
            }
            
        }
    }

};